# Notes to self - 

This web-page can be viewed at  
https://dmay.bitbucket.io/docs

## Making a document

1.  Use Madoko
2.  Typset your document using the online editor (https://www.madoko.net)
3.  Cut and paste the content from the online editor into a MarkDown file (source.mdk)

## Making the html

1. Get the Madoko command line tool (https://www.npmjs.com/package/madoko)  
2. Generate the html  
`madoko -v source.mdk --odir=autogen-output`
3. Fetch the html and put it somewhere BB will render  
`cp autogen-output/source.html index.html`
4. Push changes to the Git repo  
`git add index.html`  
`git commit -m "updated webpage"`  
`git push`